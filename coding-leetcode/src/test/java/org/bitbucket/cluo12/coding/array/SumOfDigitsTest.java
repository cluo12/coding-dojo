package org.bitbucket.cluo12.coding.array;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class SumOfDigitsTest {

    @Test
    public void testSolution () {

        int[] digits = {3,5,1};
        int sum = SumOfDigits.solution(digits);
        Assertions.assertThat(sum).isEqualTo(9);
    }

}