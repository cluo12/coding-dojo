package org.bitbucket.cluo12.coding.string;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertThat;

/**
 *
 */
public class WordCountTest
{

    private static final String TEST_PHRASE_ONE = "This sentence is not that long, that sentence is very long this";
    private WordCount wordCount;

    @Before
    public void beforeEachTest() throws Exception {
        this.wordCount = new WordCount();
    }

    @Test
    public void countWords() throws Exception
    {
        Map<String, Integer> wordCounts =
            wordCount.countWords(TEST_PHRASE_ONE);
        System.out.println(wordCounts);
        assertThat("The count for word 'long' was wrong",
            wordCounts.get("long"), CoreMatchers.is(2));
    }

    @Test
    public void countWordsInJava8() throws Exception
    {
        Map<String, Integer> wordCounts =
            wordCount.countWordsInJava8(TEST_PHRASE_ONE);
        System.out.println(wordCounts);
        assertThat("The count for word 'long' was wrong",
            wordCounts.get("long"), CoreMatchers.is(2));
    }


}