package org.bitbucket.cluo12.coding.array;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;


public class SaddleElementCounterTest {

    @Test
    public void solution() {
        int[][] numbers = {{0,1,9,3},{7,5,8,3},{9,2,9,4},{4,6,7,1}};
        Assert.assertThat("Number of saddle points is wrong", SaddleElementCounter.solution(numbers), CoreMatchers.is(2) );
    }
}