package org.bitbucket.cluo12.coding.string;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class FindFirstDuplicateCharTest
{

    private FindFirstDuplicateChar  findFirstDuplicateChar;
    @Before
    public void beforeEachTest() throws Exception {
        findFirstDuplicateChar = new FindFirstDuplicateChar();
    }

    @Test
    public void findFirstDuplicateChar() throws Exception
    {
        Optional<String> foundChar = findFirstDuplicateChar.findFirstDuplicateChar("abcdeeff");
        Assert.assertTrue(foundChar.isPresent());
        Assert.assertTrue(foundChar.get().equals("e"));
    }

    @Test
    public void notPresent_noDuplicateChars() throws Exception {
        Optional<String> foundChar = findFirstDuplicateChar.findFirstDuplicateChar("abc");
        Assert.assertFalse(foundChar.isPresent());
    }

}