package org.bitbucket.cluo12.coding.test;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;


public class FindMaxDifferenceTest {

    @Test
    public void solve() {

        List<Integer> numbers = Arrays.asList(1, 2, 6, 4, 10, 8);
        Assert.assertThat(FindMaxDifference.maxDifference(numbers.stream().mapToInt(i->i).toArray()), CoreMatchers.equalTo(9));
    }
}