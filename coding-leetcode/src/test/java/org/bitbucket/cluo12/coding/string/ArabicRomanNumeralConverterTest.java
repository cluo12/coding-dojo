package org.bitbucket.cluo12.coding.string;

import org.assertj.core.api.AssertFactory;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;


@RunWith(value = Parameterized.class)
public class ArabicRomanNumeralConverterTest {

    private Integer arabic;
    private String romanNumeral;

    public ArabicRomanNumeralConverterTest(Integer arabic, String romanNumeral) {
        this.arabic = arabic;
        this.romanNumeral = romanNumeral;
    }

    @Parameterized.Parameters (name = "{index}: {0} -> {1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {2,"II"},{5,"V"},{11,"XI"},{11,"XI"}, {41,"XLI"},{56,"LVI"},
                {98,"XCVIII"},{133,"CXXXIII"},{256,"CCLVI"},{666,"DCLXVI"},
                {800,"DCCC"},{911,"CMXI"},{1000,"M"}
        });
    }

    @Test
    public void testConvert() {
        String actualNumeral = ArabicRomanNumeralConverter.convert(this.arabic);
        Assert.assertEquals("Converted numeral is invalid", actualNumeral, romanNumeral);
    }
}