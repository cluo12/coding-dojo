package org.bitbucket.cluo12.coding.array;

import org.junit.Assert;
import org.junit.Test;

import static org.bitbucket.cluo12.coding.array.LocateNumInAnAssendingArray.solution;
import static org.hamcrest.CoreMatchers.is;


public class LocateNumInAnAssendingArrayTest {

    @Test
    public void testSolution() {
//        List<Integer> numbers = Arrays.asList(1, 2, 4, 6,7);
//        numbers.stream().mapToInt(i->i).toArray();

        int[] numbers2 = {1,2,4,6,7};

        Assert.assertThat("Array index returned is wrong", solution(numbers2,7), is(4));
        Assert.assertThat("Array index returned is wrong", solution(numbers2,4), is(2));
        Assert.assertThat("Array index returned is wrong", solution(numbers2,1), is(0));
    }
}