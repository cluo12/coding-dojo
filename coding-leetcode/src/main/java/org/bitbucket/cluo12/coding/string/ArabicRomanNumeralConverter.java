package org.bitbucket.cluo12.coding.string;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ArabicRomanNumeralConverter {
    static Logger logger = LoggerFactory.getLogger(ArabicRomanNumeralConverter.class);
    static Map<Integer, String> numberToNumeralMap = new HashMap<>();
    static {
        numberToNumeralMap.put(1, "I");
        numberToNumeralMap.put(4, "IV");
        numberToNumeralMap.put(5, "V");
        numberToNumeralMap.put(9, "IX");
        numberToNumeralMap.put(10, "X");
        numberToNumeralMap.put(40, "XL");
        numberToNumeralMap.put(50, "L");
        numberToNumeralMap.put(90, "XC");
        numberToNumeralMap.put(100, "C");
        numberToNumeralMap.put(400, "CD");
        numberToNumeralMap.put(500, "D");
        numberToNumeralMap.put(900, "CM");
        numberToNumeralMap.put(1000, "M");
    }



    public static List<String> romanizer(List<Integer> numbers) {
        if (null == numbers || numbers.size()==0){
            throw new IllegalArgumentException("Given numbers are invalid !");
        }

        if (numbers.size()>1000) {
            throw new IllegalArgumentException(String.format("number of integers received is invalid. Actual [%d], Expected: 1 <= size <= 1000",
                    numbers.size()));
        }

        List<String> result = new ArrayList<>();

        for (Integer number : numbers) {
            result.add(convert(number));
        }
        return result;
    }


    public static String convert(int numberToConvert) {

        logger.info("convert {} to roman numeral", numberToConvert );
        String result = "";

        String foundNumeral = numberToNumeralMap.get(numberToConvert);

        if (foundNumeral != null) {
            return foundNumeral;
        }

        result = parseNumber(numberToConvert);

        return result;
    }

    private static String parseNumber(Integer numberToConvert) {
        String result = "";

        List<Integer> nums = new ArrayList<>(numberToNumeralMap.keySet());
        nums.sort(Collections.reverseOrder());
        for (int num : nums) {
            while (numberToConvert >= num) {
                result += numberToNumeralMap.get(num);
                numberToConvert -= num;
            }
        }
        return result;
    }
}
