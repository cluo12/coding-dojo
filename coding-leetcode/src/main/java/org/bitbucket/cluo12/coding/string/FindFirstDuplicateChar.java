package org.bitbucket.cluo12.coding.string;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/*
 * Find first duplicate char in a string
 */
public class FindFirstDuplicateChar
{

    public Optional<String> findFirstDuplicateChar(String str) {

        if (str==null || str.length()==0) {
            return Optional.empty();
        }

        return solutionOne(str);
    }

    private Optional<String> solutionOne(String str)
    {
        Set<Character> processedCharSet = new HashSet<>();

        for(char c : str.toCharArray())
        {
             if (processedCharSet.contains(c)){
                 return Optional.of(String.valueOf(c));
             } else {
                 processedCharSet.add(c);
             }
        }

        return Optional.empty();
    }

    private Optional<String> solutionInJava8(String str)
    {

        return Optional.empty();
    }
}
