package org.bitbucket.cluo12.coding.string;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Count words in a phrase
 * An example : counting words in a file
 * https://jattardi.wordpress.com/2014/12/02/counting-words-with-java-8/
 */
public class WordCount
{

    public Map<String, Integer> countWords(String phrase)
    {
        if (phrase!=null && phrase.length()>0){
            Map<String, Integer> wordCounts = new HashMap<>();
            Pattern wordPattern = Pattern.compile("\\w+");
            Matcher matcher = wordPattern.matcher(phrase);

            while (matcher.find()){
                String word = matcher.group().toLowerCase();
                wordCounts.put(word, wordCounts.getOrDefault(word,0)+1);
            }

            return wordCounts;
        }

        return Collections.emptyMap();
    }

    public Map<String, Integer> countWordsInJava8(String phrase)
    {
         // \W+  non-words
        return Pattern.compile("\\W+")
                    .splitAsStream(phrase)
                    .map(String::toLowerCase)
                    .collect(Collectors.groupingBy(Function.identity(),
                                Collectors.reducing(0, e -> 1, Integer::sum)));
    }
}
