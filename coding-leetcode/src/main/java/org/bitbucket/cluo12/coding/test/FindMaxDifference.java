package org.bitbucket.cluo12.coding.test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * This is a test relates to Array manipulation. Given an integer array, calculate the differences between current integer and ones at lower index, and then return the max difference
 *  e.g. Given an array [1,2,6,4], the max difference is 5. How to calculate:
 *  <pre>
 *  1. 1,[] -> []
 *  2. 2,[1] -> [1]
 *  3. 6,[1,2] -> [5,4]
 *  4. 4, [1,2] -> [3,2]
 *  </pre>
 */

public class FindMaxDifference {

    private final static int MAX_LENGTH = 200000;
    private final static int MAX_VALUE = 1000000;



    static int maxDifference(int[] integerArray) {
        if (integerArray.length==0 || integerArray.length > MAX_LENGTH) {
            throw new IllegalArgumentException("Invalid integer array received. Expected size: 1<=size<="+MAX_LENGTH);
        }

        if (integerArray.length==1) {
            return -1;
        }

        int index =0;
        List<Integer> maxDifferences = new ArrayList<>();
        for (int number : integerArray) {
            checkNumberRange(number);
            int[] numbersWithLowerIndex = findLowerIndexedNumbers(index++, integerArray);
            int maxDiff = maxDiff(number, numbersWithLowerIndex);
            if (maxDiff>0) {
                maxDifferences.add(maxDiff);
            }

        }

        maxDifferences.sort(Collections.reverseOrder());

        return maxDifferences.get(0);
    }

    private static int[] findLowerIndexedNumbers(int uptoIndex, int[] integerArray) {

        if (uptoIndex == 0 || integerArray.length < uptoIndex) {
            return new int[0];
        }
        return Arrays.copyOfRange(integerArray, 0, uptoIndex);
    }

    private static int maxDiff (int number, int[] candidateNumbers) {
        if (candidateNumbers.length == 0 ) return -1;

        List<Integer> differences = new ArrayList<>();
        for (int candidateNumber : candidateNumbers) {
            if (number>candidateNumber) {
                differences.add(number - candidateNumber);
            }
        }

        differences.sort(Collections.reverseOrder());

        return differences.get(0);
    }

    private static void checkNumberRange(int number) {
        if (number<1 || number>MAX_VALUE) {
            throw new IllegalArgumentException("Invalid integer found in the integer array. Expected value: 1<=value<="+MAX_VALUE);
        }
    }
}
