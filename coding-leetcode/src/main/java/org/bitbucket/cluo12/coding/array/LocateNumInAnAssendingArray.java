package org.bitbucket.cluo12.coding.array;


public class LocateNumInAnAssendingArray {
    public static int solution(int[] array, int num) {
        int len = array.length;
        if (len == 0) {
            return -1;
        }
        int pointer = 0;
        int lastNumIndex = len ;
        while (pointer < lastNumIndex) {
            int middleIndex = (pointer + lastNumIndex) / 2;
            if (array[middleIndex] > num) {
                lastNumIndex = middleIndex;
            } else {
                pointer = middleIndex;
                if (array[middleIndex] == num) break;
            }
        }
        if (array[pointer] == num) {
            return pointer;
        }
        return -1;
    }
}
