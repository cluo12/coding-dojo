package org.bitbucket.cluo12.coding.array;

public class SaddleElementCounter {
    public static int solution(int[][] matrix) {
        return countSaddlePoint(matrix, true) + countSaddlePoint(matrix, false);
    }

    private static int countSaddlePoint(int[][] matrix, boolean rowMinColMax) {
        int count =0;
        for (int i = 0; i < matrix.length-1; i++)
        {
            int rowValue = matrix[i][0];

            int colIndex = 0;

            boolean isSaddlePoint = true;

            for (int j = 1; j < matrix[i].length - 1; j++)
            {

                if (rowMinColMax){
                    // find a local min in the ith row
                    if(matrix[i][j] < rowValue)
                    {
                        rowValue = matrix[i][j];

                        colIndex = j;
                    }
                } else {
                    // find a local max in the ith row
                    if(matrix[i][j] > rowValue)
                    {
                        rowValue = matrix[i][j];

                        colIndex = j;
                    }
                }

            }
            for (int j = 0; j < matrix.length -1 ; j++)
            {
                if (rowMinColMax) {
                    // is the element local max in its column
                    if(matrix[j][colIndex] > rowValue)
                    {
                        isSaddlePoint = false;
                        break;
                    }
                } else {
                    // a local min in its column
                    if(matrix[j][colIndex] < rowValue)
                    {
                        isSaddlePoint = false;

                        break;
                    }
                }
            }

            if(isSaddlePoint)
            {
                count++;
                break;
            }
        }
        return count;
    }
}
