package org.bitbucket.cluo12.coding.array;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A non-empty array A consisting of N integers is given. This array contains a decimal representation of a number V, i.e. element A[K] contains
 * the k-th least significant digit of the decimal representation of V. <br/><br/>
 * For example, array A such that: {3,5,1} represents the number V=153. <br/>
 * Write a function that given an array A as described above, returns the sum of the digits in the decimal representation of the number 17*V. <br /><br/>
 *
 *
 *
 */
public class SumOfDigits {

    public static int solution (int[] array) {

        if (array.length==0) return 0;
        if (array.length==1) return array[0];

        List<Character> numList = Arrays.stream(array)
                .mapToObj(i->Character.forDigit(i,10))
                .collect(Collectors.toList());

        Collections.reverse(numList);

        int product = convertToNumber(numList)*17;
        char[] digitStrs = convertEachDigitToChar(product);
        return calculateSumOfDigits (digitStrs);
    }

    private static int calculateSumOfDigits(char[] digitStrs) {
        int sum =0;
        for (char digitStr : digitStrs) {
            // chars are represented by ASCII values. '0' is a char and represented by the value of 48
            // in ASCII, 0-9 is represented by the values of 48 - 57
           sum += digitStr - '0';
        }
        return sum;
    }

    private static char[] convertEachDigitToChar(int product) {
        String strForm = String.valueOf(product);
        return strForm.toCharArray();
    }

    private static int convertToNumber(List<Character> strList) {
        String str ="";
        for (char s : strList) {
            str = str + s;
        }
        return Integer.parseInt(str);
    }
}
