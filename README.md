#### Introduction

This repo is for my coding practices over problems I have got from the internet e.g. Leetcode, or technical assessments I have participated
This project contains two sub-modules: `coding-leetcode` and `coding-tech-assessment`
#### How do I get set up?

* jdk1.8 or above
* Maven 3.5 or above

#### Whom do I talk to?

Any questions or suggestions please direct them to [me](charles_luo@yahoo.com)