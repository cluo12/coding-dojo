package org.bitbucket.cluo12.coding.math;

/**
 * Q: two pounds can buy one bottle of beer (one pint); you can exchange a bottle of beer with either 4 lids or
 * two empty bottles at the counter; you have 30 pounds, how many bottles of beer can you drink at the end?
 *
 * A:
 * Purchase, Cost -> beers, empty bottles, lids
 * 1, 2 -> 1, 1, 1
 * 2, 4 -> 2, 2, 2
 *      3, 1, 3
 * 3, 6  -> 4, 2 empty, 4 lids
 *       6, 2 empty, 2 lids
 *       7, 1 empty, 3 lids
 * 4, 8 -> 8, 2 empty, 4lids
 *        10, 2 empty, 2 lids
 *         11, 1 empty, 3 lids
 * 5, 10 -> 12, 2, 4
 *           14, 2, 2
 *           15, 1, 3
 * 6, 12 -> 16, 2, 4
 *               18, 2, 2
 *                19, 1,3
 * .......
 * 15, 30 -> (15-4)*2+15*2 = 52, 2, 4
 *                 54, 2,2
 *                 55 (2*15+ (15-3)*2+1 ), 1, 3
 *
 */
public final class BeerBottles {

    private static int BEER_COST = 2;

    public static void main(String[] args) {
        BeerBottles bottleCounter = new BeerBottles();
        bottleCounter.calculate(30);
        bottleCounter.calculate(10);
        bottleCounter.calculate(8);
    }

    private void calculate(int value) {
        int emptyBottles = 0;
        int numberOfLids = 0;
        int purchases = value / BEER_COST;
        int result = 0;

        if (purchases < 3) {
            emptyBottles = purchases;
            numberOfLids = purchases;
            result = purchases;
        } else {
            emptyBottles = 1;
            numberOfLids = 3;
            result = purchases*2 + (purchases-3)*2 + 1;
        }

        System.out.println(String.format("Money I have: %d", value ));
        System.out.println(String.format("Number of bottles: %d", result ));
        System.out.println(String.format("Empty bottles left %d", emptyBottles ));
        System.out.println(String.format("Number of lids left %d", numberOfLids ));
    }

}
