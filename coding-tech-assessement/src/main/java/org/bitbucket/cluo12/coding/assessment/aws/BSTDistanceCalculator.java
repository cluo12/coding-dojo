package org.bitbucket.cluo12.coding.assessment.aws;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

/**
 * Calculate distance of two nodes in the BST (binary search tree). The distance between two values in a b.t. is
 * the minimum number of edges traversed to reach from one value to the other.
 *
 * Given a list of n unique integers, construct a BST by inserting reach integer in the given order without rebalancing
 * the tree. Then find the distance b/w the two given nodes, node 1 and node 2, of the BST. In each
 * case, either node1 or node2 is not present in the tree, return -1.
 *
 *

 */
public class BSTDistanceCalculator
{
//    private  static final Logger LOG = LoggerFactory.getLogger(BSTDistanceCalculator.class);
//
    /**
     * Constraints:
     * 0 < n < 2(32)
     * 0 <= values[i] < 2(31)
     * 0 <=i < n
     * @param values    a list of integers                             .
     * @param n  number of elements in the list
     * @param node1  the first node
     * @param node2  the second node
     * @return An integer representing the distance between node1 and node2, else
     * return -1 if either node1 or node2 is not present in the tree
     */
    public int calculateBSTDistance(int[] values, int n,int node1, int node2) {
         return 0;
    }
}
